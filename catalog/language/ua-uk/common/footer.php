<?php
$_['text_information'] = 'Інформація';
$_['text_service'] = 'Служба підтримки';
$_['text_extra'] = 'Додатково';
$_['text_contact'] = 'Контакти';
$_['text_return'] = 'Повернення товару';
$_['text_sitemap'] = 'Карта сайту';
$_['text_manufacturer'] = 'Виробники';
$_['text_voucher'] = 'Подарункові сертифікати';
$_['text_affiliate'] = 'Партнерська програма';
$_['text_special'] = 'Акційні товари';
$_['text_account'] = 'Особистий Кабінет';
$_['text_order'] = 'Історія замовлень';
$_['text_wishlist'] = 'Закладки';
$_['text_newsletter'] = 'Розсилка';
$_['text_powered'] = 'Разработанно <a href="https://golden-web.org/">GOLDEN WEB</a> &copy; 2018';
$_['text_instagram'] = '@GOLDEN WEB';
$_['text_unine'] = 'GOLDEN WEB 2018';
?>