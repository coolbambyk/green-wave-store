<?php
$_['text_information'] = 'Информация';
$_['text_service'] = 'Служба поддержки';
$_['text_extra'] = 'Дополнительно';
$_['text_contact'] = 'Связаться с нами';
$_['text_return'] = 'Возврат товара';
$_['text_sitemap'] = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher'] = 'Подарочные сертификаты';
$_['text_affiliate'] = 'Партнёрская программа';
$_['text_special'] = 'Акционные товары';
$_['text_account'] = 'Личный кабинет';
$_['text_order'] = 'История заказа';
$_['text_wishlist'] = 'Закладки';
$_['text_newsletter'] = 'Рассылка';
$_['text_powered'] = 'Разработанно <a href="https://golden-web.org/">GOLDEN WEB</a> &copy; 2018';
$_['text_instagram'] = '@GOLDEN WEB';
$_['text_unine'] = 'GOLDEN WEB 2018';
?>