<!-- <div class="aksii-title"><?php echo $heading_title; ?></div>-->
<div class="row aksii hidden-xs">
    <div class="col-xs-12 col-lg-3  vertical-center">
        <div class="width-100-p">
            <div class="aksii-title"><?php echo $heading_title; ?></div>
            <div class="aksii-podtitle"><?=$text_pod_heading_title?></div>
            <div class="strelkii">
                <a class="left carousel-control" href="#text-carousel-1" data-slide="prev">
                    <span class="glyphicon glyphicon-menu-left"></span>
                </a>
                <a class="right carousel-control" href="#text-carousel-1" data-slide="next">
                    <span class="glyphicon glyphicon-menu-right"></span>
                </a>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-lg-9">
        <div style="background: #ffffff no-repeat; background-size: cover;"
             id="text-carousel-1" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <?php $i = 0; ?>
                <?php foreach($products as $product) { ?>
                <?php $i++; ?>
                <?php if ($i % 2 != 0) { ?>
                <div class="item <?php if($i==1){ echo 'active';} ?>">
                    <div class="carousel-content">
                        <div class="row1">
                <?php  } ?>
                        <div class="col-xs-12 col-md-6 vertical-center" style="min-height: 250px;">
                                <div class="row1 aksiiya-border"  style="width: 100%;">
                                    <div class="col-xs-5">
                                        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>"
                                                                                                          alt="<?php echo $product['name']; ?>"
                                                                                                          title="<?php echo $product['name']; ?>"
                                                                                                          class="mlr-auto img-responsive"/></a></div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="product-name">
                                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                        </div>
                                        <?php if ($product['price']) { ?>
                                        <p class="price">
                                            <?php if (!$product['special']) { ?>
                                            <?php echo $product['price']; ?>
                                            <?php } else { ?>
                                            <span class="price-new"><?php echo $product['special']; ?></span> <span
                                                    class="price-old"><?php echo $product['price']; ?></span>
                                            <?php } ?>
                                            <?php if ($product['tax']) { ?>
                                            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                            <?php } ?>
                                        </p>
                                        <?php } ?>
                                        <div class="rating">
                                            <?php for ($ii = 1; $ii <= 5; $ii++) { ?>
                                            <?php if ($product['rating'] < $ii) { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                            <?php } else { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                            <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="row1 vertical-center">
                                           <div class="col-xs-9">
                                               <button class="button-buy" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i
                                                           class="fa fa-shopping-cart"></i> <span
                                                           class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                                           </div>
                                           <div class="col-xs-3">
                                               <div class="wishlist" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>"
                                                    onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i
                                                           class="fa fa-heart"></i></div>
                                           </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                <?php if ($i % 2 == 0 || count($products)==$i) { ?>
                        </div>
                    </div>
                </div>
                <?php  } ?>


                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!--
    <div class="col-sm-12">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-thumb transition">
                <div class="wishlist" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>"
                     onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i
                            class="fa fa-heart-o"></i><?php echo $button_wishlist; ?></div>
                <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>"
                                                                                  alt="<?php echo $product['name']; ?>"
                                                                                  title="<?php echo $product['name']; ?>"
                                                                                  class="img-responsive"/></a></div>
                <div class="caption">
                    <div class="product-name"><a
                                href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                    <?php if ($product['price']) { ?>
                    <p class="price">
                        <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                        <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span> <span
                                class="price-old"><?php echo $product['price']; ?></span>
                        <?php } ?>
                        <?php if ($product['tax']) { ?>
                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                        <?php } ?>
                    </p>
                    <?php } ?>
                </div>
                <div class="rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($product['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
                                class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } ?>
                    <?php } ?>
                </div>
                <button class="button-buy" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i
                            class="fa fa-shopping-cart"></i> <span
                            class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>

                <div class="button-group">
                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                </div>

            </div>
        </div>
        <?php } ?>
    </div>
-->