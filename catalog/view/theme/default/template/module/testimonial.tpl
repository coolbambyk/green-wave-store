<?php if($layout==1){ ?>
<style type="text/css">
    .horizontal-sreview .review-author {
        font-size: 14px;
    }

    .horizontal-sreview .review-date-added {
        color: #999;
        margin-left: 10px;
    }

    .horizontal-sreview-all {
        float: right;
        margin: 0 25px 0 0;
        width: 100%;
        text-align: right;
    }

    .horizontal-sreview {
        border: 1px solid #ddd;
        margin-bottom: 20px;
        overflow: auto;
    }

    .horizontal-sreview .caption {
        padding: 15px 20px;
        min-height: 100px;
    }
</style>
<?php if($heading_title){ ?>
<div class="h1 text-center"><?php echo $heading_title; ?></div>
<?php } ?>
<div class="row">
    <?php foreach ($reviews as $review) { ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="horizontal-sreview transition">
            <div class="caption review-caption">
                <span class="review-author"><?php echo $review['author']; ?></span>
                <span class="review-date-added"><?php echo $review['date_added']; ?></span>
                <div class="rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($review['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"
                                                 style='color: #FC0;'></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"
                                                 style='color: #FC0;'></i><i
                                class="fa fa-star-o fa-stack-2x"
                                style='color: #E69500;'></i></span>
                    <?php } ?>
                    <?php } ?>
                </div>
                <p><?php echo $review['text']; ?></p>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if($button_all){ ?>
    <div class="horizontal-sreview-all"><a href="<?php echo $keyword; ?>"><?php echo $button_all_text; ?></a></div>
    <?php } ?>
</div>
<?php }else{ ?>
<style type="text/css">
    .vertical-sreview .review-author {
        font-size: 14px;
    }

    .vertical-sreview .review-date-added {
        color: #999;
        margin-left: 10px;
    }

    .vertical-sreview-all {
        float: right;
        margin: 0px 25px 0px 0px;
        width: 100%;
        text-align: right;
    }

    .vertical-sreview {
        border: 1px solid #ddd;
        margin-bottom: 20px;
        overflow: auto;
    }

    .vertical-sreview .caption {
        padding: 15px 20px;
        min-height: 100px;
    }
</style>

<div style="background: url('/image/work/testimonial.png') no-repeat; background-size: cover;" id="text-carousel" class="carousel slide mb-80" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="container" style="height: 406px;">
        <div class="row vertical-center">
            <div class="col-xs-offset-1 col-xs-10 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6">
                <div class="carousel-inner">
                    <?php $i = 0; ?>
                    <?php foreach ($reviews as $review) { ?>
                    <div class="item <?php if($i==0){ echo 'active'; } ?>">
                        <div class="carousel-content">
                            <div>
                                <?php if($heading_title){ ?>
                                <div class="text-center f-24 pb-20"><?php echo $heading_title; ?></div>
                                <?php } ?>
                                <p class="f-14"><?php echo $review['text']; ?></p>
                                <div class="text-center f-16"><?php echo $review['author']; ?></div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; } ?>
                </div>
            </div>
        </div>
        <!-- Controls --> <a class="left carousel-control" href="#text-carousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#text-carousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
<!--  <?php foreach ($reviews as $review) { ?>
  <div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="vertical-sreview transition">
          <div class="caption review-caption">
              <span class="review-author"><?php echo $review['author']; ?></span>
              <span class="review-date-added"><?php echo $review['date_added']; ?></span>
              <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($review['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"
                                               style='color: #FC0'></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"
                                               style='color: #FC0;'></i><i
                              class="fa fa-star-o fa-stack-2x"
                              style='color: #E69500;'></i></span>
                  <?php } ?>
                  <?php } ?>
              </div>
              <p><?php echo $review['text']; ?></p>
          </div>
      </div>
  </div>
  <?php } ?>
  <?php if($button_all){ ?>
  <div class="vertical-sreview-all"><a href="<?php echo $keyword; ?>"><?php echo $button_all_text; ?></a></div>
  <?php } ?>
  -->

<?php } ?>