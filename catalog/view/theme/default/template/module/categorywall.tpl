<?php if(!true){ ?>
<!--<?php if (count($categories)) { ?>
	<?php if ($cover_status) { ?>
		<div class="row categorywall <?php if ($cover_status) echo 'covers'; ?> mb-30">
		  <?php foreach ($categories as $category) { ?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="categorywall_thumbnail text-center">
							<?php if ($category['children']) { ?>
							<div class="caption">
								<ul>
								<?php foreach ($category['children'] as $child) { ?>
								<li>
								<a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
								</li>
								<?php } ?>
								</ul>
							 </div>	
							<?php } ?>
						
					   
						<a rel="nofollow" href="<?php echo $category['href']; ?>"><img class="img-responsive" src="<?php echo $category['image']; ?>"></a>
						<a class="text-center category_name<?php if ($category['children']) echo ' parent'; ?>" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
					</div>
			  </div>  
		 <?php } ?>

		</div>
		<script>
		$( document ).ready(function() {

			$('.categorywall_thumbnail').hover(
				function(){
					$(this).find('.caption').slideDown(200); //.fadeIn(250)
				},
				function(){
					$(this).find('.caption').slideUp(200); //.fadeOut(205)
				}
			); 
		});
		</script>
	<?php }  else { ?>	
		<div class="row categorywall wide">
			  <?php foreach ($categories as $category) { ?>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="product-thumb transition">
						<div class="categorywall_thumbnail <?php if ($category['children']) echo ' half'; ?>">
							<a class="text-center category_name" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
							<div class="clearfix"></div>
							<a rel="nofollow" href="<?php echo $category['href']; ?>"><img class="img-responsive" src="<?php echo $category['image']; ?>"></a>
							<?php if ($category['children']) { ?>
							<div class="children">
									<ul>
									<?php foreach ($category['children'] as $child) { ?>
									<li>
									<a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?><span class="sub"><i class="fa fa-angle-right"></i></span></a>
									</li>
									<?php } ?>
									</ul>
							</div>	
						<?php } ?>
						</div>
				  </div>  
				  </div>  
			 <?php } ?>
		</div>
	<?php } ?> 
<?php } ?> 
-->
<?php } ?>

<?php if (count($categories)) { ?>
<?php if ($cover_status) { ?>
<style>
    .kategory-cart{
        max-width: 500px;
        margin-left: auto;
        margin-right: auto;
    }
    .kategory-title{
        font-size: 20px;
        font-weight: 500;
        padding-top: 20px;
        padding-bottom: 40px;
        text-transform: uppercase;
        color: #999999;
        text-align: center;
    }
    .kategory-a:hover{
        text-decoration: none!important;
    }
    .kategory-a:hover .kategory-title{
        transform: scale(1.1);
    }

    .kategory-image{
        height: 225px;
        width: 100%;
        object-fit: cover;
        object-position: center;
        display: block;
    }
</style>
<div class="row categorywall wide">
    <?php foreach ($categories as $category) { ?>
    <!--
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
         <div class="product-thumb transition">
             <div class="categorywall_thumbnail">
                 <a class="text-center category_name" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                 <div class="clearfix"></div>
                 <a rel="nofollow" href="<?php echo $category['href']; ?>"><img class="img-responsive" src="/image/<?php echo $category['full_image']; ?>"></a>
             </div>
         </div>
     </div>
     -->

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="kategory-a" href="<?php echo $category['href']; ?>">
            <div class="card box-shadow kategory-cart">
                <img class="card-img-top kategory-image"
                     alt="Thumbnail [100%x225]"
                     src="/image/<?php echo $category['full_image']; ?>"
                     data-holder-rendered="true">
                <div class="card-body">
                    <p class="kategory-title"><?php echo $category['name']; ?></p>
                </div>
            </div>
            </a>
        </div>

    <?php } ?>
</div>
<script>
    $(document).ready(function () {

        $('.categorywall_thumbnail').hover(
            function () {
                $(this).find('.caption').slideDown(200); //.fadeIn(250)
            },
            function () {
                $(this).find('.caption').slideUp(200); //.fadeOut(205)
            }
        );
    });
</script>
<?php }  else { ?>
<div class="row categorywall mb-30">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
        <div class="col-xs-12">
            <div class="categorywall_title"><?=$heading_title_home_page?></div>
        </div>
        <div class="row1">
            <?php foreach ($categories as $category) { ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-12">
                <div class="categorywall_thumbnail text-center">
                    <a rel="nofollow" style="display: block;" href="<?php echo $category['href']; ?>"><img
                                class="img-responsive" src="<?php echo $category['image']; ?>"/>
                        <div class="left-znak"><?php echo $category['name']; ?></div>
                    </a>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
        <?php echo $bestsellers; ?>
    </div>
</div>
<?php } ?>
<?php } ?>

<script>
    $(document).ready(function () {
        cols = $('#column-right, #column-left').length;

        if (cols == 2) {
            $('.categorywall > ').attr('class', 'col-lg-6 col-md-6 col-sm-12 col-xs-12');
        } else if (cols == 1) {
            $('.categorywall > div').attr('class', 'col-lg-4 col-md-4 col-sm-6 col-xs-6');
        }
        ;

    });
</script>