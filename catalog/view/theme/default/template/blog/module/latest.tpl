
<div class="h3 text-left text-uppercase laters-blog-title"><?php echo $heading_title; ?></div>
<div class="display-table article-blog-wiget">
    <?php foreach ($articles as $article) { ?>
    <div class="product-layout col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>"
             title="<?php echo $article['name']; ?>" class="blog-image-wiget"/>
        <div class="left-znak-blog">
            <a href="<?=$article['href']?>"><div class="text-blog-title-wiget"><?php echo $article['name']; ?></div></a>
            <div class="text-blog-description-wiget"><?php echo $article['description']; ?></div>
        </div>
    </div>
    <?php } ?>
</div>