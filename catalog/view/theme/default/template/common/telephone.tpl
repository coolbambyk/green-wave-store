<?php if($telephones) { ?>
<div class="dropdown dp-style">
    <a id="phonenumbertrigger" class="dropdown-toggle pull-left" data-toggle="dropdown" href="#"><i class="fa fa-phone fs-16"></i><div class="displaynonetabletka"><?=$telephones[0]['telephone'];?></div></a>
    <ul class="dropdown-menu" style=" margin-top: 3em;">
        <?php foreach($telephones as $telephone) { ?>
        <li class="col-sm-12 vertical-center" style="padding: 18px 25px;">
            <div class="col-sm-2">
                <i class="fa fa-map-marker fs-24"></i>
            </div>
            <div class="col-sm-10">
            <a class="fw-500" href="#"><?=$telephone['sity']?></a><br>
            <a class="dropdown-text" href="#"><?=$telephone['telephone']?></a><br>
            <a class="dropdown-text" href="#"><?=$telephone['address']?></a><br>
            </div>
        </li>
        <?php } ?>
    </ul>
</div>
<?php } ?>