<footer>
    <!--<?php echo $footer_html; ?> -->
    <div class="container">
        <div class="flexfooter">
            <div class="contact-block">
                <div class="footer-title">contacts</div>
                <div class="contact-location"><i class="fa fa-map-marker"></i>
                    <div class="locationfooter">R. Bosch Street, 765 Gate 10, London, UK</div>
                </div>
                <div class="contact-email"><i class="fa fa-envelope"></i>
                    <div class="emailfooter">info@example.com</div>
                </div>
                <div class="contact-number"><i class="fa fa-phone"></i>
                    <div class="numberfooter">+38 098 123 456 7 +38 098 123 456 7</div>
                </div>
            </div>
            <div class="footerline ">
                <div class="footerlineinner">

                </div>
            </div>
            <div class="footermenu-block">
                <div class="footer-title" style="padding-left: 20px;">Information</div>
                <div class="menufooter">
                    <a>About Us</a>
                    <a>About Us</a>
                    <a>About Us</a>
                    <a>About Us</a>
                    <a>About Us</a>
                    <a>About Us</a>
                    <a>About Us</a>
                    <a>About Us</a>
                </div>
            </div>
            <div class="footerline">
                <div class="footerlineinner">

                </div>
            </div>
            <div class="signup-block">
                <div class="footer-title">SUBSCRIBE NOW</div>
                <div class="footer-text">Join us for get latest updates</div>
                <div style="position:relative;width:320px;"><input id="inputnewsletteri" class="inputnewsletter" style="padding-left:20px;outline:none;" value="">
                <i id="inputnewsletter" class="fa fa-chevron-right"
                   style="    color: #63b635;
    position: absolute;
    right: 0px;
    padding: 15px;
    cursor: pointer;"></i>
                </div>
                <a class="facebook"></a>
                <a class="google+"></a>
                <a class="instagram"></a>
            </div>
        </div>
        <!--
        <div class="row">
          <?php if ($informations) { ?>
          <div class="col-sm-3">
            <h5><?php echo $text_information; ?></h5>
            <ul class="list-unstyled">
              <?php foreach ($informations as $information) { ?>
              <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
              <?php } ?>
            </ul>
          </div>
          <?php } ?>
          <div class="col-sm-3">
            <h5><?php echo $text_service; ?></h5>
            <ul class="list-unstyled">
              <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
              <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
              <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
            </ul>
          </div>
          <div class="col-sm-3">
            <h5><?php echo $text_extra; ?></h5>
            <ul class="list-unstyled">
              <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
              <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
              <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
              <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
            </ul>
          </div>
          <div class="col-sm-3">
            <h5><?php echo $text_account; ?></h5>
            <ul class="list-unstyled">
              <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
              <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
              <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
              <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
            </ul>
          </div>
        </div>
        <hr>
        <p><?php echo $powered; ?></p>
        -->
    </div>
</footer>
<div class="footer-corporate-panel">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a href="https://golden-web.org/">
                    <div class="footer-corporate-text">GOLDEN WEB 2018</div>
                </a>
            </div>
        </div>
    </div>
</div>
<div style="position: fixed; top: 0; left: 0; width: 100%;  z-index: 10;" id="newslettersuccess"
     class="alert alert-success alert-dismissible fade in" hidden="hidden"><a href="#" class="close"
                                                                              style="padding-right: 20px"
                                                                              data-dismiss="alert"
                                                                              aria-label="close">×</a> <strong
            id="success-contact-text">Newsletter success!</strong></div>
<div style="position: fixed; top: 0; left: 0; width: 100%;  z-index: 10;" id="newslettererror"
     class="alert alert-error alert-dismissible fade in" hidden="hidden"><a href="#" class="close"
                                                                            style="padding-right: 20px"
                                                                            data-dismiss="alert"
                                                                            aria-label="close">×</a> <strong
            id="error-contact-text">Newsletter error!</strong></div>
<script>
    $("#inputnewsletter").on('click', function () {
        newslettersubscription();
    })

    function newslettersubscription() {
        if ($("#inputnewsletteri").val()=="") {
            $('#newslettererror').show(1000);
            setTimeout(function () {
                $('#newslettererror').hide(1000);
            }, 5000)
        }
        else {
            $('#newslettersuccess').show(1000);
            setTimeout(function () {
                $('#newslettersuccess').hide(1000);
            }, 5000)
        }
    };
</script>
<div class="social">
    <a class="viber-pc" href="viber://chat?number=+380978261591"></a>
    <a class="viber-phone" href="viber://add?number=380978261591"></a>
    <a class="facebook-pc" target="_blank" href="https://www.facebook.com/messages/t/serdtsekol"></a>
</div>
<style>
    .viber-pc {
        background: url("/image/viber.png") no-repeat;
        background-size: contain;
        width: 60px;
        height: 60px;
        display: none;
    }

    .viber-phone {
        background: url("/image/viber.png") no-repeat;
        background-size: contain;
        width: 60px;
        height: 60px;
        display: none;
    }

    .facebook-pc {
        background: url("/image/facebook.png") no-repeat;
        background-size: contain;
        width: 55px;
        height: 55px;
        display: inline-block;
    }

    .social {
        position: fixed;
        z-index: 20;
        bottom: 20px;
        right: 30px;
        display: inline-block
    }

    @media (max-width: 1024px) {
        .viber-pc {
            display: none;
        }
    }

    @media (min-width: 1025px) {
        .viber-phone {
            display: none;
        }
    }
</style>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>