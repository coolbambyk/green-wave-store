<div id="search" class="input-group">
  <span class="input-group-btn">
    <button type="button" class="btn btn-default btn-lg searchbutton"><i class="fa fa-search"></i></button>
  </span>
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="" class="inputsearch form-control input-lg" />
</div>