<?php echo $header; ?>

<link href="/catalog/view/theme/default/stylesheet/categorywall.css " rel="stylesheet" media="screen">
<ul class="breadcrumb">     <div class="breadcrumb-header"><?php echo $heading_title; ?></div>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
</ul>
<div class="container">
    <div class="row"><?php echo $content_toptop; ?></div>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <style>
                .compare-products{
                    min-width: 171px;
                    font-size: 14px;
                    font-weight: 400;
                    color: #333333;
                }
                .sort-products{
                    min-width: 90px;
                    font-size: 14px;
                    font-weight: 400;
                    color: #333333;
                    padding: 0;
                    margin: 0;
                    margin-left: auto;
                }
                .count-view-products{
                    min-width: 90px;
                    font-size: 14px;
                    font-weight: 400;
                    color: #333333;
                    padding: 0;
                    margin: 0;
                    margin-left: auto;
                }
                .count-view-products-select{
                    max-width: 150px;
                }
                .besstelers .product-thumb{
                    overflow: unset;
                    height: 360px;

                }
                .besstelers .product-thumb img{
                    padding-left: 5px;
                    padding-right: 5px;
                }


            </style>
            <?php if ($products) { ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-8 vertical-center">
                        <a class="compare-products hidden-xs" href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a>
                        <label class="control-label sort-products" for="input-sort"><?php echo $text_sort; ?></label>
                        <select id="input-sort" class="form-control count-view-products-select" onchange="location = this.value;">
                            <?php foreach ($sorts as $sorts) { ?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                            <option value="<?php echo $sorts['href']; ?>"
                                    selected="selected"><?php echo $sorts['text']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-4 vertical-center">

                        <label class="control-label count-view-products" for="input-limit"><?php echo $text_limit; ?></label>
                        <select id="input-limit" class="form-control count-view-products-select" onchange="location = this.value;">
                            <?php foreach ($limits as $limits) { ?>
                            <?php if ($limits['value'] == $limit) { ?>
                            <option value="<?php echo $limits['href']; ?>"
                                    selected="selected"><?php echo $limits['text']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>

                </div>


            </div>
            <br/>
            <div id="products" class="row besstelers">
                <?php foreach ($products as $product) { ?>
                <div class="product-layout col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="product-thumb transition">
                        <div class="wishlist" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i><?php echo $button_wishlist; ?></div>
                        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                        <div class="caption">
                            <div class="product-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                            <?php if ($product['price']) { ?>
                            <p class="price">
                                <?php if (!$product['special']) { ?>
                                <?php echo $product['price']; ?>
                                <?php } else { ?>
                                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                <?php } ?>
                                <?php if ($product['tax']) { ?>
                                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                <?php } ?>
                            </p>
                            <?php } ?>
                        </div>


                        <div class="rating">
                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                            <?php if ($product['rating'] < $i) { ?>
                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <?php } else { ?>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <?php } ?>
                            <?php } ?>
                        </div>


                        <button class="button-buy" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>


                        <!--
                        <div class="button-group">
                        <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                        <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                        </div>
                        -->
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="row vertical-center" style="min-width: 100%">

                <div class="col-sm-6 hidden-xs text-left"><?php echo $results; ?></div>
                <div class="col-sm-6 col-xs-12 text-right"><?php echo $pagination; ?></div>
            </div>
            <?php } ?>
            <?php if (!$categories && !$products) { ?>
            <p><?php echo $text_empty; ?></p>
            <div class="buttons">
                <div class="pull-right"><a href="<?php echo $continue; ?>"
                                           class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
            <?php } ?>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>

</div>
<div class="container">
   <?php echo $content_nadfutercont; ?>
</div>

<?php echo $footer; ?>
