<?php
class ModelLocalisationTelephone extends Model
{
    public function getTelephones()
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "telephone ORDER BY sort_order, sity");
        return $query->rows;
    }
    public function addTelephone($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "telephone SET telephone = '" . $this->db->escape($data['telephone']) . "', address = '" . $this->db->escape($data['address']) . "', sity = '" . $this->db->escape($data['sity']) . "', language_id = '" . $this->db->escape($data['language_id']) . "', sort_order = '" . $this->db->escape($data['sort_order']) . "'");
    }

    public function editTelephone($telephone_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "telephone SET telephone = '" . $this->db->escape($data['telephone']) . "', address = '" . $this->db->escape($data['address']) . "', sity = '" . $this->db->escape($data['sity']) . "', language_id = '" . $this->db->escape($data['language_id']) . "', sort_order = '" . $this->db->escape($data['sort_order']) . "' WHERE telephone_id = '" . (int)$telephone_id . "'");
    }

    public function deleteTelephone($telephone_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "telephone WHERE telephone_id = " . (int)$telephone_id);
    }

    public function getTelephone($telephone_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "telephone WHERE telephone_id = '" . (int)$telephone_id . "'");
        return $query->row;
    }


    public function getTotalTelephones() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "telephone");
        return $query->row['total'];
    }
}