<?php
// Heading
$_['heading_title'] 				= 'Nowa aukcja Amazon';
$_['text_title_advanced'] 			= 'Zaawansowana aukcja';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon US';

// Buttons
$_['button_new'] 					= 'Utwórz nowy produkt';
$_['button_amazon_price'] 			= 'Pobierz cenę z Amazon';
$_['button_list'] 					= 'Wystaw na Amazon';
$_['button_remove_error'] 			= 'Usuń wiadomości o błędach';
$_['button_save_upload'] 			= 'Zapisz i wyślij';
$_['button_browse'] 				= 'Przeglądaj';
$_['button_saved_listings'] 		= 'Zobacz zapisane aukcje';
$_['button_remove_links'] 			= 'Usuń łącza';
$_['button_create_new_listing'] 	= 'Utwórz nową aukcje';

// Help
$_['help_sku'] 						= 'Unikalne identyfikatory produktów ustawione przez handlowca';
$_['help_restock_date'] 			= 'Liczba dni po której będziesz mógł wysłać jakiekolwiek przedmioty, które są już dostępne dla klienta. Ten okres nie powinien być dłuższy niż 30 dni od dnia wystawienia. W przeciwnym razie otrzymane zamówienia mogą być anulowane automatycznie.';
$_['help_sale_price'] 				= 'Cena z wyprzedaży powinna mieć datę początkową i końcową';

//Text
$_['text_products_sent'] 			= 'Produkty zostały wysłane do przetworzenia';
$_['button_view_on_amazon'] 		= 'Zobacz na Amazon';
$_['text_list'] 					= 'Wystaw na Amazon';
$_['text_new'] 						= 'Nowy';
$_['text_used_like_new'] 			= 'Używany - Jak nowy';
$_['text_used_very_good'] 			= 'Używany - w bardzo dobrym stanie';
$_['text_used_good'] 				= 'Używany - W dobrym stanie';
$_['text_used_acceptable'] 			= 'Używany - w dopuszczalnym stanie';
$_['text_collectible_like_new'] 	= 'Collectible - Like New';
$_['text_collectible_very_good'] 	= 'Collectible - Very Good';
$_['text_collectible_good'] 		= 'Collectible - Good';
$_['text_collectible_acceptable'] 	= 'Collectible - Acceptable';
$_['text_refurbished'] 				= 'Odnowiony';
$_['text_product_not_sent'] 		= 'Produkt nie został wysłany do Amazon. Powód: %s';
$_['text_not_in_catalog'] 			= 'Albo, jeśli to nie jest w katalogu&nbsp;&nbsp;&nbsp;';
$_['text_placeholder_search'] 		= 'Wpisz nazwę produktu, UPC, EAN, ISBN lub ASIN';
$_['text_placeholder_condition'] 	= 'Użyj tego pola do opisania stanu twoich produktów.';
$_['text_characters'] 				= 'znaki';
$_['text_uploaded'] 				= 'Zapisane aukcje zostały przesłane!';
$_['text_saved_local'] 				= 'Aukcja zapisana ale jeszcze nie przesłana';
$_['text_product_sent'] 			= 'Produkt został pomyślnie wysłany do Amazon.';
$_['text_links_removed'] 			= 'Łącza do produktów Amazon zostały usunięte';
$_['text_product_links'] 			= 'Łącza do produktów';
$_['text_has_saved_listings'] 		= 'Ten produkt ma jedną lub więcej zapisanych aukcji które nie zostały przesłane';
$_['text_edit_heading'] 			= 'Edytuj aukcje';

// Columns
$_['column_image'] 					= 'Zdjęcie';
$_['column_asin'] 					= 'ASIN';
$_['column_price'] 					= 'Cena';
$_['column_action'] 				= 'Akcja';
$_['column_name'] 					= 'Nazwa producenta';
$_['column_model'] 					= 'Model';
$_['column_combination'] 			= 'Variant Combination';
$_['column_sku_variant'] 			= 'Variant SKU';
$_['column_sku'] 					= 'SKU Produktu';
$_['column_amazon_sku'] 			= 'Numer magazynowy przedmiotu Amazon (SKU)';

// Entry
$_['entry_sku'] 					= 'SKU';
$_['entry_condition'] 				= 'Kondycja';
$_['entry_condition_note'] 			= 'Notatka o stanie';
$_['entry_price'] 					= 'Cena';
$_['entry_sale_price'] 				= 'Cena zniżkowa';
$_['entry_sale_date'] 				= 'Zakres dat wyprzedaży';
$_['entry_quantity'] 				= 'Ilość';
$_['entry_start_selling'] 			= 'Dostępny od';
$_['entry_restock_date'] 			= 'Data uzupełnienia zapasów';
$_['entry_country_of_origin'] 		= 'Kraj pochodzenia';
$_['entry_release_date'] 			= 'Data wydania';
$_['entry_from'] 					= 'Data od';
$_['entry_to'] 						= 'Data do';
$_['entry_product'] 				= 'Listing for product';
$_['entry_category'] 				= 'Kategoria amazon';

//Tabs
$_['tab_main'] 						= 'Główny';
$_['tab_required'] 					= 'Wymagane informacje';
$_['tab_additional'] 				= 'Dodatkowe opcje';

//Errors
$_['error_text_missing'] 			= 'Musisz wpisać jakieś informacje o wyszukiwaniu';
$_['error_data_missing'] 			= 'Brakuje wymaganych danych';
$_['error_missing_asin'] 			= 'Brak ASIN';
$_['error_marketplace_missing'] 	= 'Proszę wybierz rynek';
$_['error_condition_missing'] 		= 'Proszę wybierz stan';
$_['error_fetch'] 					= 'Nie można uzyskać danych';
$_['error_amazonus_price'] 			= 'Nie można uzyskać ceny z Amazon US';
$_['error_stock'] 					= 'Nie możesz zamieścić aukcji z zapasem produktu mniejszym niż 1';
$_['error_sku'] 					= 'Musisz wpisać SKU dla przedmiotu';
$_['error_price'] 					= 'Musisz wpisać cenę dla przedmiotu';
$_['error_connecting'] 				= 'Ostrzeżenie: Wystąpił błąd podczas łączenia z API. Proszę sprawdź twoje ustawienia rozszerzenia OpenBay Pro Amazon. Jeżeli problem nie ustąpi, skontaktuj się z pomocą techniczną.';
$_['error_required'] 				= 'To pole jest wymagane!';
$_['error_not_saved'] 				= 'Aukcja nie została zapisana. Sprawdź wprowadzone dane.';
$_['error_char_limit'] 				= 'za dużo znaków.';
$_['error_length'] 					= 'Minimalna długość to';
$_['error_upload_failed'] 			= 'Niepowodzenia podczas wysyłania produktu z SKU: "%s". Powód: "%s" Wysyłanie zostało przerwane.';
$_['error_load_nodes'] 				= 'Unable to load browse nodes';
$_['error_not_searched'] 			= 'Search for matching items before you try to list. Items must be matched against an Amazon catalog item';