<?php
// Heading
$_['heading_title'] 				= 'Zapisane oferty';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon US';

// Text
$_['text_description']              = 'To jest lista aukcji produktów które zostały zapisane i są gotowe żeby przesłać je do Amazon.';
$_['text_uploaded_alert']           = 'Zapisane aukcje zostały przesłane!';
$_['text_delete_confirm']           = 'Jesteś pewny?';
$_['text_complete']           		= 'Aukcje wysłane';

// Column
$_['column_name']              		= 'Nazwa';
$_['column_model']             		= 'Model';
$_['column_sku']               		= 'SKU';
$_['column_amazon_sku']        		= 'Numer magazynowy przedmiotu Amazon (SKU)';
$_['column_action']           		= 'Akcja';