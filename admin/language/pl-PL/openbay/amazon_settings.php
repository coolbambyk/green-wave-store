<?php
// Heading
$_['heading_title']        				= 'Ustawienia witryny Marketplace';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon EU';

// Text
$_['text_api_status']               	= 'Status połączenia API';
$_['text_api_ok']                   	= 'Połączenie OK, Autoryzacja OK';
$_['text_api_auth_error']           	= 'Połączenie OK, Autoryzacja nie powiodła się';
$_['text_api_error']                	= 'Błąd połączenia';
$_['text_order_statuses']           	= 'Statusy zamówień';
$_['text_unshipped']                	= 'Nie wysłany';
$_['text_partially_shipped']        	= 'Częściowo wysłane';
$_['text_shipped']                  	= 'Wysłano';
$_['text_canceled']                 	= 'Anulowano';
$_['text_other']                    	= 'Inny';
$_['text_marketplaces']             	= 'Rynki';
$_['text_markets']                  	= 'Choose markets from which you would like to import your orders';
$_['text_de']                       	= 'Niemcy';
$_['text_fr']                       	= 'Francja';
$_['text_it']                       	= 'Włochy';
$_['text_es']                       	= 'Hiszpania';
$_['text_uk']                       	= 'Wielka Brytania';
$_['text_setttings_updated']        	= 'Ustawienia zostały pomyślnie zaktualizowane.';
$_['text_new'] 							= 'Nowy';
$_['text_used_like_new'] 				= 'Używany - Jak nowy';
$_['text_used_very_good'] 				= 'Używany - w bardzo dobrym stanie';
$_['text_used_good'] 					= 'Używany - W dobrym stanie';
$_['text_used_acceptable'] 				= 'Używany - w dopuszczalnym stanie';
$_['text_collectible_like_new'] 		= 'Collectible - Like New';
$_['text_collectible_very_good'] 		= 'Collectible - Very Good';
$_['text_collectible_good'] 			= 'Collectible - Good';
$_['text_collectible_acceptable'] 		= 'Collectible - Acceptable';
$_['text_refurbished'] 					= 'Odnowiony';

// Error
$_['error_permission']         			= 'Nie masz dostępu do tego modułu';

// Entry
$_['entry_status']                 		= 'Status';
$_['entry_token']                    	= 'Żeton';
$_['entry_string1']              		= 'Encryption String 1';
$_['entry_string2']              		= 'Encryption String 2';
$_['entry_import_tax']               	= 'Opłata za zimportowane przedmioty';
$_['entry_customer_group']           	= 'Grupa klientów';
$_['entry_tax_percentage']           	= 'Zmodyfikuj cenę';
$_['entry_default_condition']        	= 'Default product condition type';
$_['entry_marketplace_default']			= 'Domyślny rynek';
$_['entry_notify_admin']             	= 'Powiadom admina o nowym zamówieniu';
$_['entry_default_shipping']         	= 'Domyślny typ wysyłki';

// Tabs
$_['tab_settings']            			= 'Informacje o API';
$_['tab_listing']                  		= 'Aukcje';
$_['tab_orders']                   		= 'Zamówienia';

// Help
$_['help_import_tax']          			= 'Użyte jeżeli Amazon nie zwróci informacji o opłatach';
$_['help_customer_group']      			= 'Wybierz grupę klientów aby podłączyć ich do importowanych zamówień';
$_['help_default_shipping']    			= 'Used as the pre-selected option in the bulk order update';
$_['help_entry_marketplace_default']	= 'Default marketplace for product listings and lookups';
$_['help_tax_percentage']           	= 'Percentage added to default product price';