<?php
// Heading
$_['heading_title'] 				= 'Пап';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon EU';

// Button
$_['button_load'] 					= 'Załaduj';
$_['button_link'] 					= 'Łącze';

// Text
$_['text_local'] 					= 'Lokalny';
$_['text_load_listings'] 			= 'Ładowanie wszystkich twoich aukcji z Amazon może zająć trochę czasu (nawet do 2 godzin w niektórych przypadkach). Jeżeli podłączysz przedmioty, poziom zapasów na Amazon zostanie zaktualizowany z poziomem twojego sklepu.';
$_['text_report_requested'] 		= 'Prośba o raport aukcji z Amazon została przyjęta pomyślnie';
$_['text_report_request_failed'] 	= 'Nie udało się wysłać prośby o raport aukcji';
$_['text_loading'] 					= 'Wczytywanie elementów';
$_['text_choose_marketplace'] 		= 'Wybierz rynek';
$_['text_uk'] 						= 'Wielka Brytania';
$_['text_de'] 						= 'Niemcy';
$_['text_fr'] 						= 'Francja';
$_['text_it'] 						= 'Włochy';
$_['text_es'] 						= 'Hiszpania';

// Column
$_['column_asin'] 					= 'ASIN';
$_['column_price'] 					= 'Cena';
$_['column_name'] 					= 'Nazwa';
$_['column_sku'] 					= 'SKU';
$_['column_quantity'] 				= 'Ilość';
$_['column_combination'] 			= 'Kombinacja';

// Error
$_['error_bulk_link_permission'] 	= 'Bulk linking is not available on your plan, please upgrade to use this feature.';