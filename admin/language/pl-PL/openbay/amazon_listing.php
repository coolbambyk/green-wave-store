<?php
// Heading
$_['heading_title'] 				= 'Nowa aukcja Amazon';
$_['text_title_advanced'] 			= 'Zaawansowana aukcja';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon EU';

// Buttons
$_['button_new'] 					= 'Utwórz nowy produkt';
$_['button_amazon_price'] 			= 'Pobierz cenę z Amazon';
$_['button_list'] 					= 'Wystaw na Amazon';
$_['button_remove_error'] 			= 'Usuń wiadomości o błędach';
$_['button_save_upload'] 			= 'Zapisz i wyślij';
$_['button_browse'] 				= 'Przeglądaj';
$_['button_saved_listings'] 		= 'Zobacz zapisane aukcje';
$_['button_remove_links'] 			= 'Usuń łącza';
$_['button_create_new_listing'] 	= 'Utwórz nową aukcje';

// Help
$_['help_sku'] 						= 'Unikalne identyfikatory produktów przydzielone przez handlowca';
$_['help_restock_date'] 			= 'Liczba dni po której będziesz mógł wysłać jakiekolwiek przedmioty, które są już dostępne dla klienta. Ten okres nie powinien być dłuższy niż 30 dni od dnia wystawienia. W przeciwnym razie otrzymane zamówienia mogą być anulowane automatycznie.';
$_['help_sale_price'] 				= 'Cena z wyprzedaży powinna mieć datę początkową i końcową';

//Text
$_['text_products_sent'] 			= 'Produkty zostały wysłane do przetworzenia';
$_['button_view_on_amazon'] 		= 'Zobacz na Amazon';
$_['text_list'] 					= 'Wystaw na Amazon';
$_['text_new'] 						= 'Nowy';
$_['text_used_like_new'] 			= 'Używany - Jak nowy';
$_['text_used_very_good'] 			= 'Używany - w bardzo dobrym stanie';
$_['text_used_good'] 				= 'Używany - W dobrym stanie';
$_['text_used_acceptable'] 			= 'Używany - w dopuszczalnym stanie';
$_['text_collectible_like_new'] 	= 'Collectible - Like New';
$_['text_collectible_very_good'] 	= 'Collectible - Very Good';
$_['text_collectible_good'] 		= 'Collectible - Good';
$_['text_collectible_acceptable'] 	= 'Collectible - Acceptable';
$_['text_refurbished'] 				= 'Odnowiony';
$_['text_product_not_sent'] 		= 'Produkt nie został wysłany do Amazon. Powód: %s';
$_['text_not_in_catalog'] 			= 'Albo, jeśli to nie jest w katalogu&nbsp;&nbsp;&nbsp;';
$_['text_placeholder_search'] 		= 'Wpisz nazwę produktu, UPC, EAN, ISBN lub ASIN';
$_['text_placeholder_condition'] 	= 'Użyj tego pola do opisania stanu twoich produktów.';
$_['text_characters'] 				= 'znaki';
$_['text_uploaded'] 				= 'Zapisane aukcje zostały przesłane!';
$_['text_saved_local'] 				= 'Aukcja zapisana ale jeszcze nie przesłana';
$_['text_product_sent'] 			= 'Produkt został pomyślnie wysłany do Amazon.';
$_['text_links_removed'] 			= 'Łącza do produktów Amazon zostały usunięte';
$_['text_product_links'] 			= 'Łącza do produktów';
$_['text_has_saved_listings'] 		= 'Ten produkt ma jedną lub więcej zapisanych aukcji które nie zostały przesłane';
$_['text_edit_heading'] 			= 'Edytuj aukcje';
$_['text_germany'] 					= 'Niemcy';
$_['text_france'] 					= 'Francja';
$_['text_italy'] 					= 'Włochy';
$_['text_spain'] 					= 'Hiszpania';
$_['text_united_kingdom'] 			= 'Wielka Brytania';

// Columns
$_['column_image'] 					= 'Zdjęcie';
$_['column_asin'] 					= 'ASIN';
$_['column_price'] 					= 'Cena';
$_['column_action'] 				= 'Akcja';
$_['column_name'] 					= 'Nazwa producenta';
$_['column_model'] 					= 'Model';
$_['column_combination'] 			= 'Variant Combination';
$_['column_sku_variant'] 			= 'Variant SKU';
$_['column_sku'] 					= 'SKU Produktu';
$_['column_amazon_sku'] 			= 'Numer magazynowy przedmiotu Amazon (SKU)';

// Entry
$_['entry_sku'] 					= 'SKU';
$_['entry_condition'] 				= 'Kondycja';
$_['entry_condition_note'] 			= 'Notatka o stanie';
$_['entry_price'] 					= 'Cena';
$_['entry_sale_price'] 				= 'Cena zniżkowa';
$_['entry_sale_date'] 				= 'Zakres dat wyprzedaży';
$_['entry_quantity'] 				= 'Ilość';
$_['entry_start_selling'] 			= 'Dostępny od';
$_['entry_restock_date'] 			= 'Data uzupełnienia zapasów';
$_['entry_country_of_origin'] 		= 'Kraj pochodzenia';
$_['entry_release_date'] 			= 'Data wydania';
$_['entry_from'] 					= 'Data od';
$_['entry_to'] 						= 'Data do';
$_['entry_product'] 				= 'Listing for product';
$_['entry_category'] 				= 'Kategoria amazon';
$_['entry_browse_node'] 			= 'Choose browse node';
$_['entry_marketplace'] 			= 'Rynek';

//Tabs
$_['tab_main'] 						= 'Główny';
$_['tab_required'] 					= 'Wymagane informacje';
$_['tab_additional'] 				= 'Additional options';

// Error
$_['error_required'] 				= 'This field is required!';
$_['error_not_saved'] 				= 'Listing was not saved. Check you have filled in all fields';
$_['error_char_limit'] 				= 'characters over the limit';
$_['error_length'] 					= 'Minimum length is';
$_['error_upload_failed'] 			= 'Failed uploading product with SKU: "%s". Reason: "%s" Uploading process canceled.';
$_['error_load_nodes'] 				= 'Unable to load browse nodes';
$_['error_connecting'] 				= 'There was problem connecting to the API. Please check your OpenBay Pro Amazon extension settings. If the problem persists, please contact support.';
$_['error_text_missing'] 			= 'You must enter some search details';
$_['error_missing_asin'] 			= 'ASIN is missing';
$_['error_marketplace_missing'] 	= 'Please select a marketplace';
$_['error_condition_missing'] 		= 'Please select condition';
$_['error_amazon_price'] 			= 'Could not get the price from Amazon';
$_['error_stock'] 					= 'You cannot list an item with less than 1 item in stock';
$_['error_sku'] 					= 'You must enter an SKU for the item';
$_['error_price'] 					= 'You must enter a price for the item';
$_['error_sending_products'] 		= 'Could not send products for listing. Please contact support';
$_['error_no_products_selected'] 	= 'No products were selected for listing';
$_['error_not_searched'] 			= 'Search for matching items before you try to list. Items must be matched against an Amazon catalog item';