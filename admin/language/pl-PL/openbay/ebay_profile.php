<?php
// Heading
$_['heading_title']                     = 'Profile';
$_['text_openbay']                      = 'OpenBay Pro';
$_['text_ebay']                         = 'eBay';

//Tabs
$_['tab_returns']          				= 'Zwroty';
$_['tab_template']         				= 'Szablon';
$_['tab_gallery']          				= 'Galeria';
$_['tab_settings']         				= 'Ustawienia';

//Shipping Profile
$_['text_shipping_dispatch_country']    = 'Wysyłka z kraju';
$_['text_shipping_postcode']            = 'Kod pocztowy miejsca';
$_['text_shipping_location']            = 'Lokacja miasta lub województwa';
$_['text_shipping_despatch']            = 'Despatch time';
$_['text_shipping_despatch_help']       = 'This is the maximum number of days you will take to send the item';
$_['text_shipping_nat']                 = 'National shipping services';
$_['text_shipping_intnat']              = 'International shipping services';
$_['text_shipping_first']               = 'Pierwszy przedmiot';
$_['text_shipping_add']                 = 'Dodatkowe przedmioty';
$_['text_shipping_service']             = 'Obsługa klienta';
$_['text_shipping_in_desc']             = 'Freight info in description';
$_['text_shipping_getitfast']           = 'Get It Fast!';
$_['text_shipping_zones']               = 'Ship to zones';
$_['text_shipping_worldwide']           = 'Światowo';
$_['text_shipping_type_nat']           	= 'National shipping type';
$_['text_shipping_type_int']           	= 'International shipping type';
$_['text_shipping_flat']           		= 'Flat rate';
$_['text_shipping_calculated']          = 'Obliczone';
$_['text_shipping_freight']          	= 'Freight';
$_['text_shipping_handling']          	= 'Handling fee';
$_['text_shipping_cod']           		= 'Cash on delivery fee';
$_['text_shipping_handling_nat']    	= 'Handling fee (national)';
$_['entry_shipping_handling_int']    	= 'Handling fee (international)';

//Returns profile
$_['text_returns_accept']       		= 'Zwroty akceptowane';
$_['text_returns_inst']         		= 'Zasady zwrotów';
$_['text_returns_days']         		= 'Dni na zwrot';
$_['text_returns_days10']       		= '10 Dni';
$_['text_returns_days14']       		= '14 Dni';
$_['text_returns_days30']       		= '30 Dni';
$_['text_returns_days60']       		= '60 Dni';
$_['text_returns_type']         		= 'Typ zwrotu';
$_['text_returns_type_money']   		= 'Zwrot pieniędzy';
$_['text_returns_type_exch']    		= 'Zwrot pieniędzy lub wymiana';
$_['text_returns_costs']        		= 'Return Shipping costs';
$_['text_returns_costs_b']      		= 'Płaci kupujący';
$_['text_returns_costs_s']      		= 'Płaci sprzedający';
$_['text_returns_restock']      		= 'Restocking Fee';

//Template profile
$_['text_template_choose']      		= 'Szablon domyślny';
$_['text_template_choose_help'] 		= 'A default template will auto load when listing to save time';
$_['text_image_gallery']        		= 'Gallery image size';
$_['text_image_gallery_help']   		= 'Pixel size of gallery images that are added to your template.';
$_['text_image_thumb']          		= 'Thumbnail image size';
$_['text_image_thumb_help']     		= 'Pixel size of thumbnail images that are added to your template.';
$_['text_image_super']          		= 'Zdjęcia Super wielkości';
$_['text_image_gallery_plus']   		= 'Galeria plus';
$_['text_image_all_ebay']       		= 'Dodaj wszystkie zdjęcia na eBay';
$_['text_image_all_template']   		= 'Dodaj wszystkie zdjęcia do szablonu';
$_['text_image_exclude_default']		= 'Exclude default image';
$_['text_image_exclude_default_help']	= 'Only for bulk listing feature! Will not include the default product image in theme image list';
$_['text_confirm_delete']       		= 'Are you sure you want to delete the profile?';
$_['text_width']      					= 'Szerokość';
$_['text_height']      					= 'Wysokość';
$_['text_px']      						= 'piks';

//General profile
$_['text_general_private']      		= 'Wystaw przedmioty jako prywatną aukcję';
$_['text_general_price']        		= 'Price % modification';
$_['text_general_price_help']   		= '0 is default, -10 will reduce by 10%, 10 will increase by 10% (only used on bulk listing)';

//General profile options
$_['text_profile_name']         		= 'Nazwa';
$_['text_profile_default']      		= 'Domyślnie';
$_['text_profile_type']         		= 'Typ';
$_['text_profile_desc']         		= 'Opis';
$_['text_profile_action']       		= 'Akcja';

// Profile types
$_['text_type_shipping']       			= 'Dostawa';
$_['text_type_returns']       			= 'Zwroty';
$_['text_type_template']       			= 'Template &amp; gallery';
$_['text_type_general']       			= 'General settings';

//Success messages
$_['text_added']                		= 'New profile has been added';
$_['text_updated']              		= 'Profile has been updated';

//Errors
$_['error_permission']        			= 'You do not have permission to edit profiles';
$_['error_name']           				= 'You must enter a profile name';
$_['error_no_template']          		= 'Template ID does not exist';
$_['error_missing_settings'] 			= 'You cannot add,edit or delete profiles until you syncronise your eBay settings';