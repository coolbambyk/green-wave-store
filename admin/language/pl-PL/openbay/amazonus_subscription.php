<?php
// Heading
$_['heading_title']        				= 'Subskrypcja';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon US';

// Text
$_['text_current_plan']             	= 'Aktualny plan';
$_['text_register_invite']          	= 'Nie posiadasz jeszcze informacji API?';
$_['text_available_plans']          	= 'Dostępne plany';
$_['text_listings_remaining']       	= 'Oczekujące aukcje';
$_['text_listings_reserved']        	= 'Produkty są przetwarzane';
$_['text_account_status']           	= 'Status konta';
$_['text_merchantid']               	= 'ID Handlowca';
$_['text_change_merchantid']        	= 'Zmień';
$_['text_allowed']                  	= 'Zezwolono';
$_['text_not_allowed']              	= 'Nie zezwolono';
$_['text_price']              			= 'Cena';
$_['text_name']              			= 'Nazwa';
$_['text_description']              	= 'Opis';
$_['text_order_frequency']          	= 'Order import frequency';
$_['text_bulk_listing']             	= 'Lista zbiorowa';
$_['text_product_listings']         	= 'Aukcje przez miesiąc';

// Columns
$_['column_name']                     	= 'Nazwa';
$_['column_description']              	= 'Opis';
$_['column_order_frequency']          	= 'Order import frequency';
$_['column_bulk_listing']             	= 'Lista zbiorowa';
$_['column_product_listings']         	= 'Aukcje przez miesiąc';
$_['column_price']                    	= 'Cena';

// Buttons
$_['button_change_plan']              	= 'Zmień ofertę';
$_['button_register']                 	= 'Rejestracja';