<?php
// Heading
$_['heading_title']         		= 'eBay';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_dashboard']				= 'Panel eBay';

// Text
$_['text_success']         			= 'Zapisano zmiany do rozszerzenia eBay';
$_['text_heading_settings']         = 'Ustawienia';
$_['text_heading_sync']             = 'Synchronizuj';
$_['text_heading_subscription']     = 'Zmień ofertę';
$_['text_heading_usage']          	= 'Zużycie';
$_['text_heading_links']            = 'Odnośniki do OpenBay';
$_['text_heading_item_import']      = 'Importuj przedmioty';
$_['text_heading_order_import']     = 'Importuj zamówienia';
$_['text_heading_adds']             = 'Zainstalowane dodatki';
$_['text_heading_summary']          = 'podsumowanie eBay';
$_['text_heading_profile']          = 'Profile';
$_['text_heading_template']         = 'Szablony';
$_['text_heading_ebayacc']          = 'Konto eBay';
$_['text_heading_register']         = 'Zarejestruj się tutaj';

// Error
$_['error_category_nosuggestions']  = 'Nie można załadować żadnych proponowanych kategorii';
$_['error_loading_catalog']         = 'eBay catalog search failed';
$_['error_generic_fail']         	= 'Wystąpił nieznany błąd!';