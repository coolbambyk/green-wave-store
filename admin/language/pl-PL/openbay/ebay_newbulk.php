<?php
//Heading
$_['text_page_title']               = 'Lista zbiorowa';
$_['text_ebay']               		= 'eBay';
$_['text_openbay']               	= 'OpenBay Pro';

// Buttons
$_['text_none']                     = 'Żaden';
$_['text_preview']                  = 'Podgląd';
$_['text_add']                      = 'Dodaj';
$_['text_preview_all']              = 'Zweryfikuj wszystko';
$_['text_submit']                   = 'Prześlij';
$_['text_features']                 = 'Funkcje';
$_['text_catalog']                  = 'Wybierz katalog';
$_['text_catalog_search']           = 'Przeszukaj katalog';
$_['text_search_term']           	= 'Szukaj fraze';
$_['text_close']           			= 'Zamknij';

//Form options / text
$_['text_pixels']                   = 'Piksele';
$_['text_other']                    = 'Inny';

//Profile names
$_['text_profile']            		= 'Profile';
$_['text_profile_theme']            = 'Theme profile';
$_['text_profile_shipping']         = 'Shipping profile';
$_['text_profile_returns']          = 'Returns profile';
$_['text_profile_generic']          = 'Generic profile';

//Text
$_['text_title']                    = 'Tytuł';
$_['text_price']                    = 'Cena';
$_['text_stock']                    = 'Zapasy';
$_['text_search']                   = 'Szukaj';
$_['text_loading']                  = 'Ładowanie szczegółów';
$_['text_preparing0']               = 'Przygotowywanie';
$_['text_preparing1']               = 'z';
$_['text_preparing2']               = 'elementy';
$_['entry_condition']                = 'Kondycja';
$_['text_duration']                 = 'Czas trwania';
$_['text_category']                 = 'Kategoria';
$_['text_exists']                   = 'Some items are already listed on eBay so have been removed';
$_['text_error_count']              = 'You have selected %s items, it may take a while to process your data';
$_['text_verifying']                = 'Weryfikowanie przedmiotów';
$_['text_processing']               = 'Processing <span id="activeItems"></span> items';
$_['text_listed']                   = 'Item listed! ID: ';
$_['text_ajax_confirm_listing']     = 'Are you sure you want to bulk list these items?';
$_['text_bulk_plan_error']          = 'Your current plan does not allow for bulk uploads, upgrade your plan <a href="%s">here</a>';
$_['text_item_limit']               = 'You cannot list these items as you would exceed your plan limit, upgrade your plan <a href="%s">here</a>';
$_['text_search_text']              = 'Enter some search text';
$_['text_catalog_no_products']      = 'No catalog items found';
$_['text_search_failed']            = 'Wyszukiwanie nie powiodło się';
$_['text_esc_key']                  = 'The splash page has been hidden but may not have finished loading';
$_['text_loading_categories']       = 'Ładowanie kategorii';
$_['text_loading_condition']        = 'Loading product conditions';
$_['text_loading_duration']         = 'Loading listing durations';
$_['text_total_fee']         		= 'Opłata całkowita';
$_['text_category_choose']         	= 'Znajdź kategorię';
$_['text_suggested']         		= 'Sugerowane kategorie';

//Errors
$_['text_error_ship_profile']       = 'You need to have a default shipping profile set up';
$_['text_error_generic_profile']    = 'You need to have a default generic profile set up';
$_['text_error_return_profile']     = 'You need to have a default return profile set up';
$_['text_error_theme_profile']      = 'You need to have a default theme profile set up';
$_['text_error_variants']           = 'Items with variations cannot be bulk listed and have been unselected';
$_['text_error_stock']              = 'Some items are not in stock and have been removed';
$_['text_error_no_product']         = 'There is no eligible products selected to use the bulk upload feature';
$_['text_error_reverify']           = 'There was an error, you should edit and re-verify the items';
$_['error_missing_settings']   = 'You cannot bulk list items until you syncronise your eBay settings';
$_['text_error_no_selection']   	= 'You must select at least 1 item to list';