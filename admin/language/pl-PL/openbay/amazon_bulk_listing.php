<?php
// Heading
$_['heading_title'] 				= 'Lista zbiorowa';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon EU';

// Text
$_['text_searching'] 				= 'Wyszukiwanie';
$_['text_finished'] 				= 'Zakończono';
$_['text_marketplace'] 				= 'Rynek';
$_['text_de'] 						= 'Niemcy';
$_['text_fr'] 						= 'Francja';
$_['text_es'] 						= 'Hiszpania';
$_['text_it'] 						= 'Włochy';
$_['text_uk'] 						= 'Wielka Brytania';
$_['text_dont_list'] 				= 'Do not list';
$_['text_listing_values'] 			= 'Listing values';
$_['text_new'] 						= 'Nowy';
$_['text_used_like_new'] 			= 'Używany - Jak nowy';
$_['text_used_very_good'] 			= 'Używany - w bardzo dobrym stanie';
$_['text_used_good'] 				= 'Używany - W dobrym stanie';
$_['text_used_acceptable'] 			= 'Używany - w dopuszczalnym stanie';
$_['text_collectible_like_new'] 	= 'Collectible - Like New';
$_['text_collectible_very_good'] 	= 'Collectible - Very Good';
$_['text_collectible_good'] 		= 'Collectible - Good';
$_['text_collectible_acceptable'] 	= 'Collectible - Acceptable';
$_['text_refurbished'] 				= 'Odnowiony';

// Entry
$_['entry_condition'] 				= 'Kondycja';
$_['entry_condition_note'] 			= 'Notatka o stanie';
$_['entry_start_selling'] 			= 'Rozpocznij sprzedaż';

// Column
$_['column_name'] 					= 'Nazwa';
$_['column_image'] 					= 'Zdjęcie';
$_['column_model'] 					= 'Model';
$_['column_status'] 				= 'Status';
$_['column_matches']				= 'Dopasowania';
$_['column_result'] 				= 'Wynik';

// Button
$_['button_list'] 					= 'Lista';

// Error
$_['error_product_sku'] 			= 'Produkt musi mieć jednostkę magazynową (SKU)';
$_['error_searchable_fields'] 		= 'Produkt musi mieć wypełnione pole ISBN, UPC lub JAN';
$_['error_bulk_listing_permission'] = 'Bulk listing is not allowed on your plan, please upgrade';
$_['error_select_items'] 			= 'Musisz wybrać co najmniej 1 przedmiot do wyszukania';