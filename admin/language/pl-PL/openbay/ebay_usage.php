<?php
// Headings
$_['heading_title']             = 'Zużycie';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_ebay']                 = 'eBay';

// Errors
$_['error_ajax_load']      		= 'Przepraszam, nie można było pobrać odpowiedzi, Spróbuj ponownie później.';