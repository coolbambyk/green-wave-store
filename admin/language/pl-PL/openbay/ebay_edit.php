<?php
// Heading
$_['heading_title']					= 'Popraw aukcje eBay';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_ebay']						= 'eBay';

// Text
$_['text_revise']               	= 'Popraw aukcje';
$_['text_loading']                  = 'Pobieranie informacji o przedmiocie z eBay';
$_['text_error_loading']            = 'Wystąpił błąd podczas pobierania informacji z eBay';
$_['text_saved']                    = 'Aukcja została zapisana';
$_['text_alert_removed']            = 'The listing has been unlinked';
$_['text_alert_ended']              = 'Aukcja została zakończona na eBay';

// Buttons
$_['button_view']					= 'Zobacz aukcję';
$_['button_remove']					= 'Usuń link';
$_['button_end']                    = 'Zakończ aukcje';
$_['button_retry']					= 'Spróbuj ponownie';

// Entry
$_['entry_title']					= 'Tytuł';
$_['entry_price']					= 'Cena sprzedaży (zawiera jakiekolwiek opłaty)';
$_['entry_stock_store']				= 'Local stock';
$_['entry_stock_listed']			= 'zasoby eBay';
$_['entry_stock_reserve']			= 'Reserve level';
$_['entry_stock_matrix_active']		= 'Stock matrix (active)';
$_['entry_stock_matrix_inactive']	= 'Stock matrix (inactive)';

// Column
$_['column_sku']					= 'Var code / SKU';
$_['column_stock_listed']			= 'Listed';
$_['column_stock_reserve']			= 'Reserve';
$_['column_stock_total']			= 'Na stanie';
$_['column_price']					= 'Cena';
$_['column_status']					= 'Aktywny';
$_['column_add']					= 'Dodaj';
$_['column_combination']			= 'Kombinacja';

// Help
$_['help_stock_store']				= 'To jest poziom zasobów w OpenCart';
$_['help_stock_listed']				= 'To jest aktualny poziom zasobów na eBay';
$_['help_stock_reserve']			= 'To jest maksymalny poziom zasobów na eBay (0 = brak limitu rezerwacji)';

// Error
$_['error_ended']					= 'Załączona aukcja została zakończona, nie możesz jej edytować. Powinieneś usunąć łącze.';
$_['error_reserve']					= 'You cannot set the reserve higher than the local stock';