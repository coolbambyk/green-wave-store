<?php
// Heading
$_['heading_title']					= 'Odnośniki do OpenBay';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_amazon']					= 'Amazon EU';

// Text
$_['text_desc1']                    = 'Łączenie twoich przedmiotów pozwoli uzyskać kontrolę nad twoimi aukcjami Amazon.';
$_['text_desc2'] 					= 'For each item that is updated the local stock (the stock available in your OpenCart store) will update your Amazon listing';
$_['text_desc3']                    = 'Możesz łączyć przedmioty ręcznie poprzez wprowadzenie numeru magazynowego Amazon (SKU) oraz nazwę produktu lub załadować wszystkie niepołączone produkty a następnie wpisać numery magazynowe (Podczas przenoszenia produktów z OpenCart do Amazon łącza zostaną dodane automatycznie)';
$_['text_new_link']                 = 'Nowy link';
$_['text_autocomplete_product']     = 'Product (Auto complete from name)';
$_['text_amazon_sku']               = 'Numer magazynowy przedmiotu Amazon (SKU)';
$_['text_action']                   = 'Akcja';
$_['text_linked_items']             = 'Połączone przedmioty';
$_['text_unlinked_items']           = 'Niepołączone przedmioty';
$_['text_name']                     = 'Nazwa';
$_['text_model']                    = 'Model';
$_['text_combination']              = 'Kombinacja';
$_['text_sku']                      = 'SKU';
$_['text_amazon_sku']               = 'Numer magazynowy przedmiotu Amazon (SKU)';

// Button
$_['button_load']                 	= 'Załaduj';

// Error
$_['error_empty_sku']        		= 'Numer magazynowy Amazon (SKU) nie może być pusty!';
$_['error_empty_name']       		= 'Nazwa produktu nie może być pusta!';
$_['error_no_product_exists']       = 'Produkt nie istnieje. Użyj wartości autouzupełniania.';